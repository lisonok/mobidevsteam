import ReactDOM from 'react-dom';
import React from 'react';
import { Router, IndexRoute, Route, hashHistory } from 'react-router';

import App from './components/App';
import MainPage from './components/MainPage';
import ImagePage from './components/ImagePage';

ReactDOM.render(
    <Router history={hashHistory}>
      <Route component={App} path='/'>
        <IndexRoute component={MainPage} />
        <Route component={MainPage} path='page/:pageId' />
        <Route component={ImagePage} path='image' />
      </Route>
    </Router>,
    document.getElementById('react-view')
);
