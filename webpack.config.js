global.Promise = require('bluebird');

const webpack  = require('webpack');

let plugins = [];
if (process.env.NODE_ENV === 'production') {
  plugins.push(new webpack.optimize.DedupePlugin());
  plugins.push(new webpack.optimize.OccurenceOrderPlugin());
  // Сжимаем если production
  plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        drop_console: true,
        unsafe: true
      }
    })
  );
}

module.exports = {
  entry: {
    'bundle': ['babel-polyfill', './app.js'],
  },
  debug: process.env.NODE_ENV !== 'production',
  plugins,
  output: {
    path: `${__dirname}/`,
    filename: '[name].js',
    publicPath: '/'
  },
  module: {
    loaders: [
      { test: /\.jsx?$/, loader: 'babel', exclude: [/node_modules/, /public/] }
    ]
  },
  devtool: process.env.NODE_ENV !== 'production' ? 'source-map' : null,
  devServer: {
    headers: { 'Access-Control-Allow-Origin': '*' }
  }
};
