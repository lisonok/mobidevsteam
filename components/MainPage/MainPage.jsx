import React, { Component } from 'react';
import { Link } from 'react-router';
import axios from 'axios';

class MainPage extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);

    this.state = {
      countPage: 0,
      loading: true,
      images: [],
      error: ''
    }

    this.updatePage = this.updatePage.bind(this);
  }

  componentDidMount() {
    this.updatePage(this.props.params.pageId);
  }

  updatePage(page=1) {
    const _this = this;
    _this.setState({loading: true});
    const url = 'https://api.500px.com/v1/photos?feature=popular&consumer_key=wB4ozJxTijCwNuggJvPGtBGCRqaZVcF6jsrzUadF';
    axios.get(url, {params: {page}}).then((res) => {
      _this.setState({loading: false, error: ''});
      if (res.data && res.data.photos) {
        this.setState({images: res.data.photos, countPage: res.data.total_pages});
      }
    }).catch(function (err) {
      _this.setState({loading: false, error: err});
    });
  }

  render() {

    let pages = [];
    let pageMin = (this.props.params.pageId || 1)-5;
    if (pageMin < 1) pageMin = 1;
    let pageMax = pageMin+10;
    if (pageMax > this.state.countPage) pageMax = this.state.countPage;
    for (let i = pageMin; i <= pageMax; i++) {
      pages.push(<Link onClick={() => this.updatePage(i)}
        to={'/page/'+i} key={i} style={{
        margin: '5px',
        padding: '5px',
        background: '#aaaaaa',
        color: '#000000',
        display: 'inline-block'
      }}>{i}</Link>);
    }

    return (
      <div>
        {
          this.state.loading ?
            <h1>Загружаем...</h1>
          :
            this.state.error ?
              <h1>{this.state.error}</h1>
              :
              this.state.images.map((image, index) => {
                return (
                  <Link to={{ pathname: '/image/', query: {url: image.image_url} }}
                    key={index} style={{display: 'inline-block', margin: '20px'}}>
                    <img src={image.image_url} />
                    <br/>
                    <span>{image.user.fullname}</span>
                  </Link>
                );
              })
        }
        <div style={{width: '100%'}}>{pages}</div>
      </div>
    );
  }
}

export default MainPage;
