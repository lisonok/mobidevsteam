import React, { Component } from 'react';
import { Link } from 'react-router';

class ImagePage extends Component {
  render() {
    const url = this.props.location.query.url;
    return (
      <div>
        <Link to='/'>На главную</Link><br/>
        {
          url ?
            <img src={url} />
            :
            <h1>Изображение не найдено</h1>
        }
      </div>
    );
  }
}

export default ImagePage;
